<?php

namespace App\Http\Controllers;

use App\TaskStatus;
use Illuminate\Http\Request;

class TaskStatusesController extends Controller
{

    public function index()
    {
        $statuses = TaskStatus::orderBy('sort', 'ASC')->get();
        return view('admin.statuses', compact('statuses'));
    }

    public function create()
    {
        return view('admin.status_add');
    }

    public function store(Request $request)
    {

        TaskStatus::create([
            'title' => $request->input('title'),
            'sort' => $request->input('sort'),
        ]);

        return redirect('admin/statuses');
    }

    public function edit($id)
    {
        $status = TaskStatus::find($id);

        return view('admin.status_edit', compact('status'));
    }

    public function update(Request $request, $id)
    {
        $status = TaskStatus::find($id);

        $status->title = $request->title;
        $status->sort = $request->sort;

        $status->save();

        return redirect('admin/statuses');
    }

    public function destroy($id)
    {
        $task = TaskStatus::find($id);
        $task->delete();
        return redirect('admin/statuses');
    }

}
