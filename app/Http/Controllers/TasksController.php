<?php

namespace App\Http\Controllers;

use App\Task;
use App\TaskStatus;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    public function compose(View $view)
    {

        if (request()->has('title')) {
            $tasks = Task::where('title', request('title'))->get();
        } elseif (request()->has('created_at')) {
            $tasks = Task::where('created_at', request('created_at'))->get();
        } elseif (request()->has('deadline')) {
            $tasks = Task::where('deadline', request('deadline'))->get();
        } else {
            $tasks = Task::all();
        }

        if (request()->has('status_title')) {
            $statuses = TaskStatus::all();
            $status_id = TaskStatus::where('title', '=', request('status_title'))->get()->pluck('id');
            $tasks = Task::where('status_id', '=', $status_id)->get();
        } else {
            $statuses = TaskStatus::all();

        }

        $view->with('tasks', $tasks) && $view->with('statuses', $statuses);
    }


    public function view($id)
    {
        $task = Task::find($id);
        $statuses = TaskStatus::all();
        return view('task_view', compact('task', 'statuses'));
    }

    /**
     * Show the tasks dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index_admin()
    {
        $tasks = Task::all();
        $statuses = TaskStatus::all();
        return view('admin.tasks', compact('tasks', 'statuses'));
    }

    public function create()
    {
        $statuses = TaskStatus::all();
        return view('admin.task_add', compact('statuses'));
    }

    public function store(Request $request)
    {

        Task::create([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'deadline' => $request->input('deadline'),
            'status_id' => $request->input('status'),
        ]);

        return redirect('admin/tasks');
    }

    public function edit($id)
    {
        $task = Task::find($id);
        $statuses = TaskStatus::all();
        return view('admin.task_edit', compact('task', 'statuses'));
    }

    public function update(Request $request, $id)
    {
        $task = Task::find($id);

        $task->title = $request->title;
        $task->description = $request->description;
        $task->deadline = $request->deadline;
        $task->status_id = $request->status;

        $task->save();

        return redirect('admin/tasks');
    }

    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();
        return redirect('admin/tasks');
    }
}
