<?php

namespace App;

use App\Http\Controllers\TasksController;
use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{


    protected $table = 'task_statuses';

    protected $id = 'id';

    protected $fillable = [
        'title',
        'sort',
    ];

    public function task()
    {
        return $this->hasMany(TasksController::class, 'status_id', 'id');
    }

}
