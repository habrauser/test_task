<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{

    protected $table = 'tasks';

    protected $id = 'id';

    protected $fillable = [
        'title',
        'description',
        'deadline',
    ];


}
