<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'task'], function() {
    Route::get('/{id}/view', 'TasksController@view')->name('task.view');
});


Auth::routes();


Route::group(['prefix' => 'admin'], function() {
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/tasks', 'TasksController@index_admin')->name('tasks');
        Route::group(['prefix' => 'task'], function () {
            Route::get('/create', 'TasksController@create')->name('task.create');
            Route::post('/store', 'TasksController@store')->name('task.store');
            Route::get('/{id}/edit', 'TasksController@edit')->name('task.edit');
            Route::post('/{id}/update', 'TasksController@update')->name('task.update');
            Route::get('/{id}/delete', 'TasksController@destroy')->name('task.delete');
        });
        Route::group(['prefix' => 'statuses'], function () {
            Route::get('/', 'TaskStatusesController@index')->name('statuses');
            Route::get('/create', 'TaskStatusesController@create')->name('status.create');
            Route::post('/store', 'TaskStatusesController@store')->name('status.store');
            Route::get('/{id}/edit', 'TaskStatusesController@edit')->name('status.edit');
            Route::post('/{id}/update', 'TaskStatusesController@update')->name('status.update');
            Route::get('/{id}/delete', 'TaskStatusesController@destroy')->name('status.delete');
        });
    });
});
