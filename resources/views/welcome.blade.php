<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Test Task</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ route('tasks') }}">Manage Tasks</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md">
            Task List
        </div>


        <div class="container">

            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <h4>Filter:</h4>
                        <span>Title - </span>
                        @foreach($tasks as $task)
                            <a href="/?title={{$task->title}}">{{$task->title}}</a>
                        @endforeach
                        <br><span>Date - </span>
                        @foreach($tasks as $task)
                            <a href="/?created_at={{$task->created_at}}">{{$task->created_at->format('Y-m-d')}}</a>
                        @endforeach
                        <br><span>Deadline - </span>
                        @foreach($tasks as $task)
                            <a href="/?created_at={{$task->deadline}}">{{$task->deadline}}</a>
                        @endforeach
                        <br><span>Status - </span>
                        @foreach($statuses as $status)
                            <a href="/?status_title={{$status->title}}">{{$status->title}}</a>
                        @endforeach

                        <table cellspacing="20" width="100%">
                            <thead>
                            <tr>

                                <th>Title</th>
                                <th>Description</th>
                                <th>Created</th>
                                <th>Deadline</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody id="DataTable">
                            @foreach($tasks as $task)
                                <tr>

                                    <td>{{$task->title}}</td>
                                    <td>{{str_limit($task->description, 15, '...')}}</td>
                                    <td>{{$task->created_at->format('Y-m-d')}}</td>
                                    <td>{{$task->deadline}}</td>
                                    <td>@foreach($statuses as $status)
                                            @if($status->id === $task->status_id)
                                                {{$status->title}}
                                            @endif
                                        @endforeach</td>
                                    <td>
                                        <a href="{{route('task.view', $task->id)}}" target="_blank" title="View"><i
                                                    class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>
                        </table>


                    </div>
                </div>

            </div>


        </div>

    </div>
</div>
</body>
</html>
