@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add New Task Status</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="{{route('status.store')}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input class="form-control" name="title" placeholder="Title" required><br>
                                <input class="form-control" type="number" name="sort" placeholder="Sort number" required><br>

                            </div>

                            <button type="submit" class="btn btn-primary">Add</button>
                        </form>


                    </div>
                </div>
            </div>
        </div>




    </div> </div>


    </main>
@endsection
