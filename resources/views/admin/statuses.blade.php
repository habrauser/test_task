@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <main class='main-content bgc-grey-100'>
        <div id='mainContent'>

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Tasks Statuses List</div>

                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif


                                <a href="{{ route('status.create') }}"><i class="fas fa-plus"></i> Add new status</a>

                                <table id="dtOrderExample" class="table table-striped table-bordered" cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Sort Number</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($statuses as $status)
                                        <tr>
                                            <td>{{$status->id}}</td>
                                            <td>{{$status->title}}</td>
                                            <td>{{$status->sort}}</td>
                                            <td>
                                                <a href="{{route('status.edit', $status->id)}}" title="Edit"><i
                                                            class="fas fa-edit"></i></a>
                                                <a href="{{route('status.delete', $status->id)}}" title="Delete"><i
                                                            class="fas fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </main>
@endsection