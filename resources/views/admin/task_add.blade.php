@extends('layouts.app')

@section('content')

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Add New Task</div>

                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                    <form method="post" action="{{route('task.store')}}">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <input class="form-control" name="title" placeholder="Title" required><br>
                                            <textarea class="form-control" name="description" placeholder="Description" required></textarea>
                                            <p>Deadline: <input class="form-control" type="date" name="deadline"></p>
                                            <select class="form-control" name="status">
                                                @foreach($statuses as $status)
                                                    <option value="{{$status->id}}">{{$status->title}}</option>
                                                @endforeach
                                            </select>


                                        </div>

                                        <button type="submit" class="btn btn-primary">Add</button>
                                    </form>


                            </div>
                        </div>
                    </div>
                </div>




            </div> </div>


    </main>
@endsection
