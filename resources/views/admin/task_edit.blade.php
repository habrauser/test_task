@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Task</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="{{route('task.update', $task->id)}}">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input class="form-control" name="title" value="{{$task->title}}" required><br>
                                <textarea class="form-control" name="description"
                                          required>{{$task->description}}</textarea>
                                <p>Deadline: <input class="form-control" type="date" name="deadline"
                                                    value="{{$task->deadline}}"></p>
                                <select class="form-control" name="status">
                                    @foreach($statuses as $status)
                                        <option value="{{$status->id}}">{{$status->title}}</option>
                                    @endforeach
                                </select>


                            </div>

                            <button type="submit" class="btn btn-primary">Update</button>
                        </form>


                    </div>
                </div>
            </div>
        </div>


    </div> </div>


    </main>
@endsection
