@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

    <main class='main-content bgc-grey-100'>
        <div id='mainContent'>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tasks List</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <a href="{{ route('task.create') }}"><i class="fas fa-plus"></i> Add new task</a>

                        <table id="dtOrderExample" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Created</th>
                                <th>Deadline</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>

                            <tbody id="DataTable">
                            @foreach($tasks as $task)
                                <tr>
                                    <td>{{$task->id}}</td>
                                    <td>{{$task->title}}</td>
                                    <td>{{str_limit($task->description, 15, '...')}}</td>
                                    <td>{{$task->created_at->format('Y-m-d')}}</td>
                                    <td>{{$task->deadline}}</td>
                                    <td>@foreach($statuses as $status)
                                        @if($status->id === $task->status_id)
                                    {{$status->title}}
                                        @endif
                                    @endforeach</td>
                                    <td>
                                        <a href="{{route('task.edit', $task->id)}}" title="Edit"><i class="fas fa-edit"></i></a>
                                        <a href="{{route('task.delete', $task->id)}}" title="Delete"><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody></table>



                </div>
            </div>
        </div>
    </div>




</div> </div>

        <script>
            $(document).ready(function () {
                $('#dtOrderExample').DataTable({
                    columnDefs: [{
                        orderable: false,
                        targets: 3
                    }]
                });
                $('.dataTables_length').addClass('bs-select');
            });
        </script>

    </main>
@endsection