@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="card">
            <h1>Title: {{$task->title}}</h1>
            <h3>Description: <br>{{$task->description}}</h3>
            <h3>Created: {{$task->created_at}}</h3>
            <h3>Deadline: {{$task->deadline}}</h3>
            <h4>Status: @foreach($statuses as $status)
                    @if($status->id === $task->status_id)
                        {{$status->title}}
                    @endif
                @endforeach</h4>
        </div>
    </div>

@endsection